<?php
namespace Knightcms\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function posts()
    {
        return $this->belongsToMany('Knightcms\Blog\Models\Post');
    }

    public function getposts()
    {
        return $this->belongsToMany('Knightcms\Blog\Models\Post')->where([['public', '=', 1],['public_at', '<=', date('Y-m-d H:i:s')]]);
    }
}
