<?php

namespace Knightcms\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class PostUser extends Model
{
    protected $table = 'users';
}
