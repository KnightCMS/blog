<?php

namespace Knightcms\Blog;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Events\Dispatcher;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class BlogServiceProvider extends ServiceProvider
{

    private $models = [
        'Category',
        'Post',
        'PostUser',
        'Tag',
    ];

    public function boot()
    {
        try {
            $this->loadModels();
            $this->loadViewsFrom(__DIR__ . '/../resources/views', 'blog');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function register()
    {
        if (request()->is(config('voyager.prefix')) || request()->is(config('voyager.prefix') . '/*')) {

            app(Dispatcher::class)->listen('voyager.menu.display', function ($menu) {
                $this->addBlogMenuItems($menu);
            });

            // DB and BREAD create
            $this->addBlogTable();
            $this->CreateDataType();
            $this->CreateDataRow();

        }

        // publish config
        $this->publishes([dirname(__DIR__) . '/config/blog.php' => config_path('blog.php')], 'knightcms-blog-config');

        // Register Blog Facade
        $loader = AliasLoader::getInstance();
        $loader->alias('Blog', \Knightcms\Blog\Facades\Blog::class);

        $this->app->singleton('blog', function () {
            return new Blog();
        });
        // Blog Facade register end
    }

    private function CreateDataType()
    {
        $this->InsertDataType('posts', 'posts', 'Bejegyzés',
            'Bejegyzések', 'voyager-news', 'Knightcms\\Blog\\Models\\Post',
            1, '\\Knightcms\\Blog\\Http\\Controllers\\PostController', 'Knightcms\\Blog\\Policies\\PostPolicy');

        $this->InsertDataType('tags', 'tags', 'Címke',
            'Címkék', 'voyager-tag', 'Knightcms\\Blog\\Models\\Tag',
            1);

        $this->InsertDataType('categories', 'categories', 'kategória',
            'kategóriák', 'voyager-categories', 'Knightcms\\Blog\\Models\\Category',
            1);
    }

    private function InsertDataType($slug, $name, $display_name_singular,
        $display_name_plural, $icon, $model_name,
        $generate_permissions, $controller = null, $policy_name = null, $description = null) {
        $dataType = DB::table('data_types')->where('slug', $slug)->first();
        if ($dataType === null) {
            DB::table('data_types')->insert([
                'slug' => $slug,
                'name' => $name,
                'display_name_singular' => $display_name_singular,
                'display_name_plural' => $display_name_plural,
                'icon' => $icon,
                'model_name' => $model_name,
                'policy_name' => $policy_name,
                'controller' => $controller,
                'generate_permissions' => $generate_permissions,
                'description' => $description,
            ]);
            if ($generate_permissions == 1) {
                $this->PermissionGenerateFor($slug);
            }
        }
    }

    private function CreateDataRow()
    {
        $this->InsertPostDataRows();
        $this->InsertCategoryRows();
        $this->InsertTagsRows();
    }

    protected function InsertPostDataRows()
    {
        $postDataType = DB::table('data_types')->where('slug', 'posts')->first();

        $this->InsertDataRow($postDataType, 'id',
            ['type' => 'number',
                'display_name' => 'Azonosító',
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'details' => '',
                'order' => 1,
            ]);

        $this->InsertDataRow($postDataType, 'author_id', [
            'type' => 'text',
            'display_name' => 'Szerző azonosítója',
            'required' => 1,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => '',
            'order' => 2,
        ]);

        $this->InsertDataRow($postDataType, 'title', [
            'type' => 'text',
            'display_name' => 'Bejegyzés címe',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '',
            'order' => 3,
        ]);

        $dataRow = $this->InsertDataRow($postDataType, 'body', [
            'type' => 'rich_text_box',
            'display_name' => 'Tartalom',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 5,
        ]);

        $this->InsertDataRow($postDataType, 'image', [
            'type' => 'image',
            'display_name' => 'Kiemelt kép',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => json_encode([
                'resize' => [
                    'width' => '1000',
                    'height' => 'null',
                ],
                'quality' => '70%',
                'upsize' => true,
                'thumbnails' => [
                    [
                        'name' => 'medium',
                        'scale' => '50%',
                    ],
                    [
                        'name' => 'small',
                        'scale' => '25%',
                    ],
                    [
                        'name' => 'cropped',
                        'crop' => [
                            'width' => '300',
                            'height' => '250',
                        ],
                    ],
                ],
            ]),
            'order' => 6,
        ]);

        $this->InsertDataRow($postDataType, 'slug', [
            'type' => 'text',
            'display_name' => 'SEO URL',
            'required' => 1,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => json_encode([
                'slugify' => [
                    'origin' => 'title',
                    'forceUpdate' => true,
                ],
                'validation' => [
                    'rule' => 'unique:posts,slug',
                ],
            ]),
            'order' => 7,
        ]);

        $this->InsertDataRow($postDataType, 'meta_description', [
            'type' => 'text_area',
            'display_name' => 'Meta leírás',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 8,
        ]);

        $this->InsertDataRow($postDataType, 'meta_keywords', [
            'type' => 'text_area',
            'display_name' => 'Meta kulcsszavak',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 9,
        ]);

        $this->InsertDataRow($postDataType, 'status', [
            'type' => 'select_dropdown',
            'display_name' => 'Státusz',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => json_encode([
                'default' => 'DRAFT',
                'options' => [
                    'PUBLISHED' => 'Közzétéve',
                    'DRAFT' => 'Vázlat',
                    'PENDING' => 'Függőben',
                ],
            ]),
            'order' => 10,
        ]);

        $this->InsertDataRow($postDataType, 'excerpt', [
            'type' => 'text_area',
            'display_name' => 'Bevezető',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 11,
        ]);

        $this->InsertDataRow($postDataType, 'excerpt_silder', [
            'type' => 'text_area',
            'display_name' => 'Silder bevezető',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 12,
        ]);

        $this->InsertDataRow($postDataType, 'seo_title', [
            'type' => 'text',
            'display_name' => 'SEO cím',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 13,
        ]);

        $this->InsertDataRow($postDataType, 'featured', [
            'type' => 'checkbox',
            'display_name' => 'Kiemelt',
            'required' => 1,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '',
            'order' => 14,
        ]);

        $dataRow = $this->InsertDataRow($postDataType, 'meta_image', [
            'type' => 'image',
            'display_name' => 'SEO kiemelt kép',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 15]);

        $this->InsertDataRow($postDataType, 'meta_social_description', [
            'type' => 'text',
            'display_name' => 'Közösségi média leírás',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 16,
        ]);

        $dataRow = $this->InsertDataRow($postDataType, 'public_at', [
            'type' => 'timestamp',
            'display_name' => 'Publikálás dátuma',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => '{"null": ""}',
            'order' => 17]);

        $dataRow = $this->InsertDataRow($postDataType, 'created_at', [
            'type' => 'timestamp',
            'display_name' => 'Létrehozás dátuma',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => '',
            'order' => 18]);

        $this->InsertDataRow($postDataType, 'updated_at', [
            'type' => 'timestamp',
            'display_name' => 'Frissítés dátuma',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => '',
            'order' => 19,
        ]);

        $this->InsertDataRow($postDataType, 'post_belongsto_user_relationship', [
            'type' => 'relationship',
            'display_name' => 'Szerző',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => json_encode([
                'model' => 'Knightcms\\Blog\\Models\\PostUser',
                'table' => 'users',
                'type' => 'belongsTo',
                'column' => 'author_id',
                'key' => 'id',
                'label' => 'name',
                'pivot_table' => 'users',
                'pivot' => '0',
            ]),
            'order' => 20,
        ]);

    }

    protected function InsertCategoryRows()
    {
        $categoryType = DB::table('data_types')->where('slug', 'categories')->first();

        $this->InsertDataRow($categoryType, 'id',
            ['type' => 'number',
                'display_name' => 'Azonosító',
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'details' => '',
                'order' => 1,
            ]);

        $this->InsertDataRow($categoryType, 'name', [
            'type' => 'text',
            'display_name' => 'Kategória neve',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '',
            'order' => 2,
        ]);

        $this->InsertDataRow($categoryType, 'slug', [
            'type' => 'text',
            'display_name' => 'SEO URL',
            'required' => 1,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => json_encode([
                'slugify' => [
                    'origin' => 'name',
                    'forceUpdate' => true,
                ],
                'validation' => [
                    'rule' => 'unique:categories,slug',
                ],
            ]),
            'order' => 3,
        ]);

        $this->InsertDataRow($categoryType, 'meta_description', [
            'type' => 'text_area',
            'display_name' => 'Meta leírás',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 4,
        ]);

        $this->InsertDataRow($categoryType, 'meta_keywords', [
            'type' => 'text_area',
            'display_name' => 'Meta kulcsszavak',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 5,
        ]);

        $this->InsertDataRow($categoryType, 'seo_title', [
            'type' => 'text',
            'display_name' => 'SEO cím',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 6,
        ]);

        $dataRow = $this->InsertDataRow($categoryType, 'meta_image', [
            'type' => 'image',
            'display_name' => 'SEO kiemelt kép',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 8]);

        $this->InsertDataRow($categoryType, 'meta_social_description', [
            'type' => 'text_area',
            'display_name' => 'Közösségi média leírás',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '{"null": ""}',
            'order' => 7,
        ]);

        $dataRow = $this->InsertDataRow($categoryType, 'created_at', [
            'type' => 'timestamp',
            'display_name' => 'Létrehozás dátuma',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => '',
            'order' => 9]);

        $this->InsertDataRow($categoryType, 'updated_at', [
            'type' => 'timestamp',
            'display_name' => 'Frissítés dátuma',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => '',
            'order' => 10,
        ]);

    }

    protected function InsertTagsRows()
    {

        $tagsType = DB::table('data_types')->where('slug', 'tags')->first();

        $this->InsertDataRow($tagsType, 'id',
            ['type' => 'number',
                'display_name' => 'Azonosító',
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'details' => '',
                'order' => 1,
            ]);

        $this->InsertDataRow($tagsType, 'name', [
            'type' => 'text',
            'display_name' => 'Címke neve',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => '',
            'order' => 2,
        ]);

        $this->InsertDataRow($tagsType, 'slug', [
            'type' => 'text',
            'display_name' => 'SEO URL',
            'required' => 1,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => json_encode([
                'slugify' => [
                    'origin' => 'name',
                    'forceUpdate' => true,
                ],
                'validation' => [
                    'rule' => 'unique:tags,slug',
                ],
            ]),
            'order' => 3,
        ]);

        $dataRow = $this->InsertDataRow($tagsType, 'created_at', [
            'type' => 'timestamp',
            'display_name' => 'Létrehozás dátuma',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => '',
            'order' => 4]);

        $this->InsertDataRow($tagsType, 'updated_at', [
            'type' => 'timestamp',
            'display_name' => 'Frissítés dátuma',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => '',
            'order' => 5,
        ]);
    }

    /**
     * Adds the Theme icon to the admin menu.
     *
     * @param TCG\Voyager\Models\Menu $menu
     */
    protected function addBlogMenuItems(Menu $menu)
    {
        if ($menu->name == 'admin') {
            $parentID = $this->ParentMenu($menu);
            $this->MenuItem($menu, 'voyager.posts.index', 'Bejegyzések', 'voyager-news', $parentID, 1);
            $this->MenuItem($menu, 'voyager.categories.index', 'Kategóriák', 'voyager-categories', $parentID, 2);
            $this->MenuItem($menu, 'voyager.tags.index', 'Címkék', 'voyager-tag', $parentID, 3);
        }
    }

    private function MenuItem($menu, $route, $name, $icon, $parentID, $order)
    {
        $url = route($route, [], false);
        $menuItem = $menu->items->where('url', $url)->first();
        if (is_null($menuItem)) {
            $menu->items->add(MenuItem::create([
                'menu_id' => $menu->id,
                'url' => $url,
                'title' => $name,
                'target' => '_self',
                'icon_class' => $icon,
                'color' => null,
                'parent_id' => $parentID,
                'order' => $order,
            ]));
            return redirect()->back();
        }
    }

    private function ParentMenu($menu)
    {
        $menuItem = $menu->items->where('title', 'Blog')->first();
        if (is_null($menuItem)) {
            $menu->items->add(MenuItem::create([
                'menu_id' => $menu->id,
                'url' => '',
                'title' => 'Blog',
                'target' => '_self',
                'icon_class' => 'voyager-pen',
                'color' => null,
                'parent_id' => null,
                'order' => 4,
            ]));
            return $menuItem = $menu->items->where('title', 'Blog')->first()->id;
        }
    }

    /**
     * Loads all models in the src/Models folder.
     *
     * @return none
     */
    private function loadModels()
    {
        foreach ($this->models as $model) {
            @include __DIR__ . '/Models/' . $model . '.php';
        }
    }

    /**
     * Add the necessary Themes tables if they do not exist.
     */
    private function addBlogTable()
    {
        if (!Schema::hasTable('posts')) {
            Schema::create('posts', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('author_id');
                $table->string('title');
                $table->string('seo_title')->nullable();
                $table->text('excerpt')->nullable();;
                $table->text('body')->nullable();;
                $table->string('image')->nullable();
                $table->string('slug')->unique();
                $table->text('meta_description')->nullable();;
                $table->text('meta_keywords')->nullable();;
                $table->enum('status', ['PUBLISHED', 'DRAFT', 'PENDING'])->default('DRAFT');
                $table->boolean('featured')->default(0);
                $table->text('excerpt_silder')->nullable();
                $table->dateTime('public_at')->nullable();
                $table->string('meta_image')->nullable();
                $table->text('meta_social_description')->nullable();
                $table->timestamps();
                $table->foreign('author_id')->references('id')->on('users');
            });
        }
        if (!Schema::hasTable('categories')) {
            Schema::create('categories', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('slug')->unique();
                $table->string('seo_title')->nullable();
                $table->text('meta_description')->nullable();;
                $table->text('meta_keywords')->nullable();;
                $table->string('meta_image')->nullable();
                $table->text('meta_social_description')->nullable();
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('tags')) {
            Schema::create('tags', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('slug')->unique();
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('category_post')) {
            Schema::create('category_post', function (Blueprint $table) {
                $table->integer('post_id')->unsigned()->index();
                $table->foreign('post_id')->references('id')->on('posts')->onUpdate('cascade')->onDelete('cascade');
                $table->integer('category_id')->unsigned()->index();
                $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
            });
        }
        if (!Schema::hasTable('post_tag')) {
            Schema::create('post_tag', function (Blueprint $table) {
                $table->integer('post_id')->unsigned()->index();
                $table->foreign('post_id')->references('id')->on('posts')->onUpdate('cascade')->onDelete('cascade');
                $table->integer('tag_id')->unsigned()->index();
                $table->foreign('tag_id')->references('id')->on('tags')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }

// Duplicating the rescue function that's available in 5.5, just in case
    // A user wants to use this hook with 5.4

    public function rescue(callable $callback, $rescue = null)
    {
        try {
            return $callback();
        } catch (Throwable $e) {
            report($e);
            return value($rescue);
        }
    }

    protected function InsertDataRow($type, $field, $insert_data)
    {
        $querry = DB::table('data_rows')->where([['data_type_id', $type->id], ['field', $field]])->first();
        if ($querry === null) {
            $insert_data = array_merge(array('data_type_id' => $type->id, 'field' => $field), $insert_data);
            DB::table('data_rows')->insert($insert_data);
        }
    }

    protected function PermissionGenerateFor($table_name)
    {
        $getAdminRoles = DB::table('roles')->where('name', 'admin')->first();

        $browse = $this->firstOrCreate('permissions', ['key' => 'browse_' . $table_name, 'table_name' => $table_name]);
        $this->firstOrCreate('permission_role', ['permission_id' => $browse->id, 'role_id' => $getAdminRoles->id]);

        $read = $this->firstOrCreate('permissions', ['key' => 'read_' . $table_name, 'table_name' => $table_name]);
        $this->firstOrCreate('permission_role', ['permission_id' => $read->id, 'role_id' => $getAdminRoles->id]);

        $edit = $this->firstOrCreate('permissions', ['key' => 'edit_' . $table_name, 'table_name' => $table_name]);
        $this->firstOrCreate('permission_role', ['permission_id' => $edit->id, 'role_id' => $getAdminRoles->id]);

        $add = $this->firstOrCreate('permissions', ['key' => 'add_' . $table_name, 'table_name' => $table_name]);
        $this->firstOrCreate('permission_role', ['permission_id' => $add->id, 'role_id' => $getAdminRoles->id]);

        $delete = $this->firstOrCreate('permissions', ['key' => 'delete_' . $table_name, 'table_name' => $table_name]);
        $this->firstOrCreate('permission_role', ['permission_id' => $delete->id, 'role_id' => $getAdminRoles->id]);
    }

    private function firstOrCreate($table, $where, $insert = null)
    {
        $querry = DB::table($table)->where($where)->first();
        if ($querry === null) {
            if ($insert == null) {
                $insert = $where;
            }
            DB::table($table)->insert($insert);
            return DB::table($table)->where($where)->first();
        }
        return $querry;
    }
}
