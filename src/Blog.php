<?php

namespace Knightcms\Blog;

use Knightcms\Blog\Models\Category;
use Knightcms\Blog\Models\Tag;

class Blog
{
    public function getAllCategory()
    {
        return Category::all();
    }

    public function getAllTags()
    {
        return Tag::all();
    }
}