@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('css')
    <style>


        /* Image widget */
        .image-wrapper.add-edit {
            position: relative;
            text-align: center;
        }

        .image-wrapper.add-edit .overlay {
            position: absolute;
            color: #eee;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.3);
            transition: background-color 0.3s ease-in-out;
            display: flex;
            flex-direction: column;
            justify-content: center;
            cursor: pointer;
            font-size: 25px;
            line-height: 1;
        }

        .image-wrapper.add-edit .overlay:hover {
            background-color: rgba(0, 0, 0, 0.5);
        }

        .image-wrapper.add-edit .overlay i {
            opacity: 0.7;
            font-size: 150%;
        }

        .image-wrapper.add-edit .overlay p {
            opacity: 0.7;
        }

        /* End Image widget */

        .panel .mce-panel {
            border-left-color: #fff;
            border-right-color: #fff;
        }

        .panel .mce-toolbar,
        .panel .mce-statusbar {
            padding-left: 20px;
        }

        .panel .mce-edit-area,
        .panel .mce-edit-area iframe,
        .panel .mce-edit-area iframe html {
            padding: 0 10px;
            min-height: 350px;
        }

        .mce-content-body {
            color: #555;
            font-size: 14px;
        }

        .panel.is-fullscreen .mce-statusbar {
            position: absolute;
            bottom: 0;
            width: 100%;
            z-index: 200000;
        }

        .panel.is-fullscreen .mce-tinymce {
            height: 100%;
        }

        .panel.is-fullscreen .mce-edit-area,
        .panel.is-fullscreen .mce-edit-area iframe,
        .panel.is-fullscreen .mce-edit-area iframe html {
            height: 100%;
            position: absolute;
            width: 99%;
            overflow-y: scroll;
            overflow-x: hidden;
            min-height: 100%;
        }

        .select2 {
            width: 100% !important;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form"
              action="@if(isset($dataTypeContent->id)){{ route('voyager.posts.update', $dataTypeContent->id) }}@else{{ route('voyager.posts.store') }}@endif"
              method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-8">
                    <!-- ### TITLE ### -->
                    <div class="panel">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="voyager-character"></i> {{ __('voyager::post.title') }}
                                <span class="panel-desc"> {{ __('voyager::post.title_sub') }}</span>
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                   aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name'  => 'title',
                                '_field_trans' => get_field_translations($dataTypeContent, 'title')
                            ])
                            <input type="text" class="form-control" id="title" name="title"
                                   placeholder="{{ __('voyager::generic.title') }}"
                                   value="@if(isset($dataTypeContent->title)){{ $dataTypeContent->title }}@endif">
                        </div>
                    </div>

                    <!-- ### CONTENT ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ __('voyager::post.content') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen"
                                   aria-hidden="true"></a>
                            </div>
                        </div>

                        <div class="panel-body">
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name'  => 'body',
                                '_field_trans' => get_field_translations($dataTypeContent, 'body')
                            ])
                            @php
                                $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                                $row = $dataTypeRows->where('field', 'body')->first();
                            @endphp
                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                        </div>
                    </div><!-- .panel -->

                    <!-- ### EXCERPT ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Bevezető</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                   aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'excerpt',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'excerpt')
                                ])
                                <label for="excerpt">Rövid tartalom / bevezető</label>
                                <textarea class="form-control" id="excerpt"
                                          name="excerpt">@if (isset($dataTypeContent->excerpt)){{ $dataTypeContent->excerpt }}@endif</textarea>
                            </div>
                            <div class="form-group">
                                @include('voyager::multilingual.input-hidden', [
                                        '_field_name'  => 'excerpt_silder',
                                        '_field_trans' => get_field_translations($dataTypeContent, 'excerpt_silder')
                                    ])
                                <label for="excerpt_silder">Silder tartalom / bevezető</label>
                                <textarea class="form-control" id="excerpt_silder"
                                          name="excerpt_silder">@if (isset($dataTypeContent->excerpt_silder)){{ $dataTypeContent->excerpt_silder }}@endif</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ __('voyager::post.additional_fields') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                   aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @php
                                $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                                $exclude = ['title', 'body', 'excerpt', 'excerpt_silder', 'slug', 'status', 'category_id', 'public_at', 'author_id', 'featured', 'image', 'meta_description', 'meta_keywords', 'seo_title', 'meta_social_description', 'meta_image'];
                            @endphp

                            @foreach($dataTypeRows as $row)
                                @if(!in_array($row->field, $exclude))
                                    @php
                                        $options = json_decode($row->details);
                                        $display_options = isset($options->display) ? $options->display : NULL;
                                    @endphp
                                    @if ($options && isset($options->formfields_custom))
                                        @include('voyager::formfields.custom.' . $options->formfields_custom)
                                    @else
                                        <div class="form-group @if($row->type == 'hidden') hidden @endif @if(isset($display_options->width)){{ 'col-md-' . $display_options->width }}@endif" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                            {{ $row->slugify }}
                                            <label for="name">{{ $row->display_name }}</label>
                                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                                            @if($row->type == 'relationship')
                                                @include('voyager::formfields.relationship')
                                            @else
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            @endif

                                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                            @endforeach
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <!-- ### DETAILS ### -->
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i> {{ __('voyager::post.details') }}
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                   aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="slug">{{ __('voyager::post.slug') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'slug',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'slug')
                                ])
                                <input type="text" class="form-control" id="slug" name="slug"
                                       placeholder="slug"
                                       {{!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug") !!}}
                                       value="@if(isset($dataTypeContent->slug)){{ $dataTypeContent->slug }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="status">{{ __('voyager::post.status') }}</label>
                                <select class="form-control" name="status">
                                    <option value="PUBLISHED"
                                            @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'PUBLISHED') selected="selected"@endif>{{ __('voyager::post.status_published') }}</option>
                                    <option value="DRAFT"
                                            @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'DRAFT') selected="selected"@endif>{{ __('voyager::post.status_draft') }}</option>
                                    <option value="PENDING"
                                            @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'PENDING') selected="selected"@endif>{{ __('voyager::post.status_pending') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="category_id">{{ __('voyager::post.category') }}</label>
                                <select multiple="" id="category_id" class="form-control" name="category[]">
                                    @foreach(Blog::getAllCategory() as $category)
                                        <option value="{{ $category->id }}"
                                                @if($dataTypeContent->category->contains($category->id)) selected="selected" @endif>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="tags_id">Címkék</label>

                                <select multiple="" id="tags_id" class="form-control" name="tags[]">
                                    @foreach($dataTypeContent->tags as $tag)
                                        <option value="{{ $tag->name}}" selected="selected">{{ $tag->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="public_at">Publikálás dátuma</label>
                                <input type="text" class="form-control datepicker" id="public_at" name="public_at"
                                       placeholder="Publikálás dátuma" autocomplete="off"
                                       value="@if(isset($dataTypeContent->public_at)){{ $dataTypeContent->public_at }}@endif">
                            </div>

                            <div class="form-group">
                                <label for="featured">{{ __('voyager::generic.featured') }}</label>
                                <input type="checkbox" name="featured"
                                       @if(isset($dataTypeContent->featured) && $dataTypeContent->featured) checked="checked"@endif>
                            </div>
                        </div>
                    </div>

                    <!-- ### IMAGE ### -->
                    <div class="panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-image"></i> {{ __('voyager::post.image') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                   aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="image-wrapper add-edit">
                                <img id="holder_image"
                                     src="{{ filter_var($dataTypeContent->image, FILTER_VALIDATE_URL) ? $dataTypeContent->image : Voyager::image( $dataTypeContent->image ) }}"
                                     style="width: 100%; min-height: 150px; background: #eee"/>
                                <input id="input_image" type="hidden" name="image"
                                       value="{{ $dataTypeContent->image }}">

                                @php $add = __('voyager::generic.add'); $update = __('voyager::generic.update'); @endphp
                                <div class="overlay" onclick="OpenImagePicker('image')">
                                    <i class="voyager-images"></i>
                                    <p class="image-action">
                                        Fénykép @if(isset($dataTypeContent->image)){{ $update }}@else{{ $add }}@endif</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ### SEO CONTENT ### -->
                    <div class="panel panel-bordered panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-search"></i> {{ __('voyager::post.seo_content') }}
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                   aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="meta_description">{{ __('voyager::post.meta_description') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'meta_description',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'meta_description')
                                ])
                                <textarea class="form-control"
                                          name="meta_description">@if(isset($dataTypeContent->meta_description)){{ $dataTypeContent->meta_description }}@endif</textarea>
                            </div>
                            <div class="form-group">
                                <label for="meta_keywords">{{ __('voyager::post.meta_keywords') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'meta_keywords',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'meta_keywords')
                                ])
                                <textarea class="form-control"
                                          name="meta_keywords">@if(isset($dataTypeContent->meta_keywords)){{ $dataTypeContent->meta_keywords }}@endif</textarea>
                            </div>
                            <div class="form-group">
                                <label for="seo_title">{{ __('voyager::post.seo_title') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'seo_title',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'seo_title')
                                ])
                                <input type="text" class="form-control" name="seo_title"
                                       placeholder="{{ __('voyager::post.seo_title') }}"
                                       value="@if(isset($dataTypeContent->seo_title)){{ $dataTypeContent->seo_title }}@endif">
                            </div>

                            <div class="form-group">
                                @include('voyager::multilingual.input-hidden', [
                                        '_field_name'  => 'meta_social_description',
                                        '_field_trans' => get_field_translations($dataTypeContent, 'meta_social_description')
                                    ])
                                <label for="meta_social_description">Közösségi média leírás</label>
                                <textarea class="form-control" id="meta_social_description"
                                          name="meta_social_description">@if (isset($dataTypeContent->meta_social_description)){{ $dataTypeContent->meta_social_description }}@endif</textarea>
                            </div>

                        </div>
                    </div>

                    <div class="panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-image"></i>SEO Kép</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                   aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <p>Megosztáskor ez a kép fog megjeleni a közösségi oldalakon</p>
                            <div class="image-wrapper add-edit">
                                <img id="holder_meta_image"
                                     src="{{ filter_var($dataTypeContent->meta_image, FILTER_VALIDATE_URL) ? $dataTypeContent->meta_image : Voyager::image( $dataTypeContent->meta_image ) }}"
                                     style="width: 100%; min-height: 150px; background: #eee"/>
                                <input id="input_meta_image" type="hidden" name="meta_image"
                                       value="{{ $dataTypeContent->meta_image }}">

                                @php $add = __('voyager::generic.add'); $update = __('voyager::generic.update'); @endphp
                                <div class="overlay" onclick="OpenImagePicker('meta_image')">
                                    <i class="voyager-images"></i>
                                    <p class="image-action">
                                        SEO
                                        kép @if(isset($dataTypeContent->meta_image)){{ $update }}@else{{ $add }}@endif</p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right">
                @if(isset($dataTypeContent->id)){{ __('voyager::post.update') }}@else <i
                        class="icon wb-plus-circle"></i> {{ __('voyager::post.new') }} @endif
            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
              enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
@stop

@section('javascript')
    @php
        $tags = "";
        foreach(Blog::getAllTags() as $tag) {
            if(!$dataTypeContent->tags->contains($tag->id)){
                $tags = $tags. "'{$tag->name}',";
            }
        }
    @endphp
    <script>
        $('document').ready(function () {
            $('#slug').slugify();
            $("#category_id").select2({
                placeholder: "Kategóri(a/ák)"
            });

            $('#public_at').datetimepicker();

            $("#tags_id").select2({
                placeholder: "Címk(e/ék)",
                tags: [{!! $tags !!}],
                tokenSeparators: [",", " "]
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif
        });

        function OpenImagePicker(input_name) {
            window.open("{{route('media.img_picker')}}?InsertImageID=" + input_name, 'ImagePickerWindow', 'width=900,height=600');
        }

    </script>
@stop
